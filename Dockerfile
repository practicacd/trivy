FROM alpine:3.18.4
RUN apk --no-cache add ca-certificates git
ENTRYPOINT ["trivy"]